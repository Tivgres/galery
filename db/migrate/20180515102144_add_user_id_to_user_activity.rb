class AddUserIdToUserActivity < ActiveRecord::Migration[5.2]
  def change
    add_column :user_activities, :user_id, :integer
  end
end
