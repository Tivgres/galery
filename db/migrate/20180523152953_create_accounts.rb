class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.string :provider
      t.string :uid
      t.integer :user_id
      t.timestamps
    end
    remove_column :users, :provider
    remove_column :users, :uid
  end
end
