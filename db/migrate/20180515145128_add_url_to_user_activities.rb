class AddUrlToUserActivities < ActiveRecord::Migration[5.2]
  def change
    add_column :user_activities, :url, :string
  end
end
