class CreateUserActivities < ActiveRecord::Migration[5.2]
  def change
    create_table :user_activities do |t|
      t.string :controller
      t.string :action
      t.timestamps
    end
  end
end
