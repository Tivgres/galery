class AddSlugToImages < ActiveRecord::Migration[5.2]
  def change
    change_table :images do |t|
      t.string :slug, after: :id
    end
  end
end
