class AddSlugToCategories < ActiveRecord::Migration[5.2]
  def change
    change_table :categories do |t|
      t.string :slug, after: :id
    end
  end
end
