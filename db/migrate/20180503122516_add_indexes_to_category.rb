class AddIndexesToCategory < ActiveRecord::Migration[5.2]
  def change
    add_column :likes, :category_id, :integer
    add_column :comments, :category_id, :integer
  end
end
