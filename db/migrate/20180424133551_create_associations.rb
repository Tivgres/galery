class CreateAssociations < ActiveRecord::Migration[5.2]
  def change
    add_index :comments, [:user_id, :image_id]
    add_index :likes, [:user_id, :image_id]
    add_column :images, :category_id, :integer
    add_index :images, [:category_id]
  end
end
