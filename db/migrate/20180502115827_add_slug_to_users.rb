class AddSlugToUsers < ActiveRecord::Migration[5.2]
  def change
    change_table :users do |t|
      t.string :slug, after: :username
    end
    add_index :users, :slug, unique: true
  end
end
