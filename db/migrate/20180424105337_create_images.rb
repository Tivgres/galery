class CreateImages < ActiveRecord::Migration[5.2]
  def change
    create_table :images do |t|
      t.integer 'user_id'
      t.string  'file_path'
      t.string  'title'
      t.timestamps
    end
  end
end
