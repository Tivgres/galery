class CreateSubscribes < ActiveRecord::Migration[5.2]
  def change
    create_table :subscribes do |t|
      t.integer :user_id
      t.integer :category_id
      t.timestamps

      remove_column :categories, :all_count
      add_column :categories, :subscribes_count, :integer, default: 0
    end
  end
end
