OmniAuth.config.logger = Rails.logger

Rails.application.config.middleware.use OmniAuth::Builder do
  provider :google_oauth2, ENV['GOOGLE_PLUS_KEY'], ENV['GOOGLE_PLUS_SECRET']
  provider :twitter, ENV['TWITTER_API_KEY'], ENV['TWITTER_API_SECRET_KEY']
  provider :github, ENV['GITHUB_ID'], ENV['GITHUB_SECRET']
end