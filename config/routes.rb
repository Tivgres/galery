# frozen_string_literal: true

Rails.application.routes.draw do
  resources :subscribes
  resources :categories, param: :slug
  resources :images, param: :slug
  resources :likes
  resources :comments
  resources :accounts
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users, param: :slug, controllers: {
      registrations: 'users/registrations', sessions: 'users/sessions'#,
      #omniauth_callbacks: 'callbacks'
  }
  get '/auth/:provider/callback', to: 'callbacks#create'
  authenticated :user do
    root 'categories#index', as: :authenticated_root
  end
  root 'welcome#index'
  mount Resque::Server, at: 'admin/resque'
  post 'language', to: 'application#language'
end
