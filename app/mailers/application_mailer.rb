class ApplicationMailer < ActionMailer::Base
  default from: 'mr.serg.d@gmail.com'
  layout 'mailer'
end
