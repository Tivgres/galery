class UserMailer < ApplicationMailer
  # include Resque::Mailer

  def subscribe_email(user, category)
    @user = user
    @category = category
    mail(to: @user.email, subject: "You subscribed to the category #{@category.title}")
  end

  def subscribe_update(users, category)
    @category = category
    email_list = users.map(&:email)
    mail(to: email_list.first, bcc: email_list[1..-1], subject: "Category #{@category.title} was updated")
  end
end
