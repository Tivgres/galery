# frozen_string_literal: true

class ImagesController < InheritedResources::Base
  before_action :authenticate_user!

  def index
    @images = Image.where.not(category_id: nil).page(params[:page]).per(15)
  end

  def show
    @image = Image.friendly.find(params[:slug])
    @comments = @image.comments.order('created_at DESC').page(params[:page]).per(10)
    @like = @image.likes.find_by(user_id: current_user.id)
  end

  def new
    @image = Image.new
    @categories = Category.friendly.all
  end

  def create
    @image = current_user.images.new(images_params)
    respond_to do |format|
      if @image.save
        format.html do
          redirect_to image_path @image, success: t('controllers.images.uploaded')
        end
        format.json do
          render json: @image, status: :created, location: @image
        end
      else
        format.html  do
          flash[:danger] = @image.errors
          redirect_to new_image_path
        end
        format.json
      end
    end
  end

  private

  def images_params
    params.require(:image).permit(:file_path, :title, :category_id)
  end
end
