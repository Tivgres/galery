# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  prepend_before_action :set_captcha, only: :new
  prepend_before_action :captcha_valid, only: :create

  # GET /resource/sign_in
  def new
    @user = User.new
    cookies[:login_attempts] ||= 0
    cookies[:login_attempts] = cookies[:login_attempts].to_i + 1
  end

  def create
    super
  end

  def destroy
    User.find(current_user.id).update!(last_sign_out_at: DateTime.now)
    super
  end

  private

  def captcha_valid
    if cookies[:login_attempts].to_i >= 3 && verify_recaptcha || cookies[:login_attempts].to_i < 3
      cookies[:login_attempts] = 0
      true
    else
      set_captcha
      self.resource = resource_class.new(sign_in_params)
      respond_with_navigational(resource) { redirect_to new_user_session_path }
    end
  end

  def set_captcha
    @captcha = cookies[:login_attempts].to_i >= 3
  end
end
