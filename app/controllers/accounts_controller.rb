class AccountsController < InheritedResources::Base
  before_action :authenticate_user!

  def destroy
    @item_id = params[:item_id]
    @account = Account.find(params[:id])
    respond_to do |format|
      if current_user.id == @account.user_id
        if @account.destroy!
          alert_success t('controllers.accounts.unpaired')
          format.js   { redirect_to edit_user_registration_path, status: :accepted }
        else
          format.js   { render status: :bad_request }
        end
      else
        format.js   { render status: :forbidden }
      end
    end
  end
end
