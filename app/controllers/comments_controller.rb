# frozen_string_literal: true

class CommentsController < InheritedResources::Base
  before_action :authenticate_user!
  before_action :comment_params, only: :create

  def index
    @comments = Comment.order('created_at DESC').page(params[:page]).per(12)
  end

  def create
    @comment = current_user.comments.new comment_params
    respond_to do |format|
      if @comment.save
        format.html { render status: :created }
        format.js   { render status: :created }
        format.json { render :show, status: :created, location: @comment }
      else
        format.html { render :new, status: :bad_request }
        format.js   { render status: :bad_request }
        format.json { render json: @comment.errors, status: :bad_request }
      end
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:image_id, :category_id, :body)
  end
end
