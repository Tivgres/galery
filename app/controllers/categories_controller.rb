# frozen_string_literal: true

class CategoriesController < InheritedResources::Base
  before_action :authenticate_user!
  before_action :find_category, except: %i[index new create]

  def index
    @categories = Category.page(params[:page]).per(6)
  end

  def show
    @images = @category.images.page(params[:page]).per(6)
    respond_with @images
  end

  def new
    @category = Category.new
  end

  def create
    @category = current_user.categories.new category_params
    respond_to do |format|
      if @category.save
        format.html do
          redirect_to(@category, success: t('controllers.categories.created'))
        end
        format.json do
          render json: @category, status: :created, location: @category
        end
      else
        format.html { render action: 'new' }
        format.json do
          render json: @category.errors, danger: :unprocessable_entity
        end
      end
    end
  end

  def edit; end

  def update
    respond_to do |format|
      if @category.user_id == current_user.id
        if @category.update!(category_params)
          alert_success t('controllers.categories.updated')
          format.html { redirect_to category_path(@category) }
        end
      else
        format.html { render :edit, status: :forbidden }
      end
    end
  end

  def delete; end

  def destroy
    respond_to do |format|
      if @category.user_id == current_user.id
        if @category.destroy!
          alert_success t('controllers.categories.deleted')
          format.html { redirect_to categories_path }
        end
      else
        format.html { render :index, status: :forbidden }
      end
    end
  end

  private

  # BEFORE ACTIONS

  def find_category
    @category = Category.friendly.find(params[:slug])
    @subscribe = @category.subscribes.find_by(user_id: current_user.id)
  end

  # PARAMS FINDER

  def category_params
    params.require(:category).permit(:title)
  end
end
