# frozen_string_literal: true

class SubscribesController < InheritedResources::Base
  before_action :authenticate_user!
  before_action :subscribe_params, only: :create

  def create
    @subscribe_old = current_user.subscribes.where(subscribe_params).first
    respond_to do |format|
      unless @subscribe_old
        @subscribe = current_user.subscribes.new subscribe_params
        if @subscribe.save
          @category = Category.find(subscribe_params[:category_id])
          format.js   { }
        end
      end
    end
  end

  def destroy
    @subscribe = Subscribe.find(params[:id])
    respond_to do |format|
      if current_user.id == @subscribe.user_id
        if @subscribe.destroy!
          @category = Category.find(params[:category_id])
          format.js   { render status: :accepted }
        else
          format.js   { render status: :bad_request }
        end
      else
        format.js   { render status: :forbidden }
      end
    end
  end

  private

  def subscribe_params
    params.require(:subscribe).permit(:category_id)
  end
end
