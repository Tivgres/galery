# frozen_string_literal: true

class LikesController < InheritedResources::Base
  before_action :authenticate_user!
  before_action :like_params, only: :create

  def create
    @like_old = Like.where(user_id: current_user.id, image_id: like_params[:image_id]).first
    respond_to do |format|
      if @like_old
        format.js     { render status: :found }
        format.json   { render status: :found }
      else
        @like = current_user.likes.new(like_params)
        if @like.save
          @image = @like.image
          format.js     { render status: :created }
          format.json   { render status: :created }
        else
          format.js   { render status: :bad_request }
        end
      end
    end
  end

  def destroy
    @like = Like.find(params[:id])
    respond_to do |format|
      if current_user.id == @like.user_id
        @image = @like.image
        if @like.destroy!
          format.js   { render status: :accepted }
        else
          format.js   { render status: :bad_request }
        end
      else
        format.js   { render status: :forbidden }
      end
    end
  end

  private

  def like_params
    params.require(:like).permit(:image_id, :category_id)
  end
end
