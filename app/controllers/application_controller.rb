# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_locale
  before_action :get_categories
  append_after_action :save_analytics

  def language
    cookies[:lang_code] = params[:lang_code]
    redirect_to request.referrer
  end

  protected

  def set_locale
    if cookies[:lang_code].nil?
      header = request.env['HTTP_ACCEPT_LANGUAGE'] || 'en'
      cookies[:lang_code] = header.scan(/^[a-z]{2}/).first
    end
    I18n.locale = cookies[:lang_code] || I18n.default_locale
    @linked_countries = %w[us ru]
    @available_languages = %w[en ru]
    @current_language = cookies[:lang_code]
    if @current_language.eql?('en')
      @current_country = 'us'
    else
      @current_country = @current_language
    end
  end

  def configure_permitted_parameters
    added_attrs = %i[username email password password_confirmation remember_me]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
  end

  def get_categories
    if signed_in?
      @categories_header = Category.order('images_count DESC', 'likes_count DESC', 'comments_count DESC').limit(5)
    end
  end

  # Fix for disable friendly_id to ActiveAdmin
  ActiveAdmin::ResourceController.class_eval do
    def find_resource
      resource_class.is_a?(FriendlyId) ? collection.where(slug: params[:id]).first! : collection.where(id: params[:id]).first!
    end
  end

  def after_sign_in_path_for(resource)
    request.env['omniauth.origin'] || stored_location_for(resource) || root_path
  end

  def alert_success(body)
    flash[:success] = body
  end

  def alert_error(body)
    flash[:danger] = body
  end

  private

  def save_analytics
    Resque.enqueue(AnalyticsSaver, params[:controller], params[:action], current_user.id, request.url) if user_signed_in?
  end

end
