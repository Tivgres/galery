ActiveAdmin.register_page "Parsed pictures" do

  content title: 'Parsed pics' do
    columns do
      column do
        panel 'Uploaded pics' do
          ul do
            @images = Image.where(category_id: nil).all.each do |image|
              @categories = Category.all
              li do
                div image_tag image.file_path.url(:small_thumb), class: 'image-show'
                form_for :image, url: admin_parsed_pictures_update_path do |f|
                  f.hidden_field :image_id, value: image.id
                  f.label 'Title'
                  br
                  f.text_area :title, value: image.title
                  br
                  f.label 'Category'
                  br
                  f.collection_select :category_id, @categories, :id, :title
                  br
                  br
                  f.submit 'Update'
                end
                form_for :image, url: admin_parsed_pictures_update_path do |f|
                  f.hidden_field :image_id, value: image.id
                  f.submit 'Delete'
                end
                br
                hr
                br
              end
            end
          end
        end
      end
    end
  end


  page_action :update, method: :post do
    @image = Image.find(params[:image][:image_id])
    @image.title = params[:image][:title]
    @image.category_id = params[:image][:category_id]
    if @image.save
      flash[:success] = 'Updated'
    else
      flash[:danger] = @image.errors
    end
    redirect_to admin_parsed_pictures_path
  end

  page_action :destroy, method: :post do
    @image = Image.find(params[:image][:image_id])
    if @image.destroy
      flash[:success] = 'Deleted'
    else
      flash[:danger] = @image.errors
    end
    redirect_to admin_parsed_pictures_path
  end

end