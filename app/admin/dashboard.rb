ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  page_action :parse_site, :method => :post do
    url =  params[:parse][:url]
    Resque.enqueue(ImageParser, url)
    flash[:success] = "Task for a parsing #{url} successfully added"
    redirect_to admin_root_path
  end

  content title: proc{ I18n.t("active_admin.dashboard") } do

    columns do
      column do
        panel 'Recent categories' do
          ul do
            Category.order('created_at DESC').limit(5).each do |category|
              li link_to(category.title, admin_category_path(category))
            end
          end
        end
      end

      column do
        panel 'Recent images' do
          ul do
            Image.order('created_at DESC').limit(10).each do |image|
              li do
                div b link_to(image.title, admin_image_path(image))
                div image_tag image.file_path.url(:small_thumb), class: 'image-show'
                br
              end
            end
          end
        end
      end

      column do
        panel 'Recent comments' do
          ul do
            Comment.order('created_at DESC').limit(5).each do |comment|
              li do
                div b link_to(comment.body, admin_comment_path(comment))
                div link_to(comment.user.username, admin_user_path(comment.user))
                div comment.created_at.strftime("%d/%m/%Y %H:%M")
                br
              end
            end
          end
        end
      end

      column do
        panel 'Images uploader' do
          form_for :parse, url: admin_dashboard_parse_site_url do |f|
            f.text_area :url, placeholder: 'Site url with images', id: 'admin-image-parser'
            f.submit 'Parse for the images'
          end
        end
      end

    end
  end
end
