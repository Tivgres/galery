class Like < ApplicationRecord
  belongs_to :user
  belongs_to :image
  belongs_to :category, counter_cache: true
  validates :image_id, :user_id, :category_id, presence: true
  validates :user_id, uniqueness: { scope: :image_id }
end
