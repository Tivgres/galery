# frozen_string_literal: true

class Image < ApplicationRecord
  extend FriendlyId
  friendly_id :title, use: :slugged
  mount_uploader :file_path, ImageUploader

  belongs_to :user
  belongs_to :category, counter_cache: true
  has_many :comments
  has_many :likes

  validates :file_path, :title, :user_id, :category_id, presence: true

  after_save :send_updates

  def send_updates
    Resque.enqueue(ImageUpdates, id)
  end
end
