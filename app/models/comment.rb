class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :image
  belongs_to :category, counter_cache: true
  validates :body, :image_id, :user_id, :category_id, presence: true
end
