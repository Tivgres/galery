class Subscribe < ApplicationRecord
  belongs_to :user
  belongs_to :category

  validates :user_id, :category_id, presence: true

  after_save :send_confirmation

  def send_confirmation
    Resque.enqueue(SubscribeNotification, id)
  end

end
