class ImageUpdates
  @queue = :images_queue

  def self.perform(id)
    category = Image.find(id).category
    UserMailer.subscribe_update(Subscribe.where(category_id: category.id).map(&:user), category).deliver_now!
  end
end