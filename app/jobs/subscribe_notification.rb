class SubscribeNotification
  @queue = :subscribes_queue

  def self.perform(subscribe_id)
    sub = Subscribe.find(subscribe_id)
    UserMailer.subscribe_email(sub.user, sub.category).deliver_now!
  end
end