require 'nokogiri'
require 'open-uri'

class ImageParser
  @queue = :image_parser_queue

  def self.perform(url)
    domain = url.split('/')[0..2].join('/').to_s
    page = Nokogiri::HTML(open(url))
    images = []
    page.xpath("//img/@src").each do |item|
      begin
        if item.to_s.end_with?('.jpg') || item.to_s.end_with?('.png') || item.to_s.end_with?('.jpeg')
          if item.to_s.include?('http')
            images << item
          else
            images << "#{domain}#{item}"
          end
        end
      rescue
        #
        #
      end
    end
    images.each do |img|
      temp_file = Tempfile.new
      temp_file.write(open(img).read.force_encoding('utf-8'))
      title = SecureRandom.uuid
      uploaded_file = ActionDispatch::Http::UploadedFile.new(tempfile: temp_file, filename: "#{title}.#{img.to_s.split('.').last}")
      image = Image.new
      image.user_id= User.first.id
      image.title= title
      image.slug= title
      image.file_path= uploaded_file
      image.save(validate: false)
    end
  end
end