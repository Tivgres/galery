class AnalyticsSaver
  @queue = :application_saver_queue

  def self.perform(controller, action, user_id, url)
    UserActivity.create(controller: controller, action: action,
                        user_id: user_id, url: url)
  end
end