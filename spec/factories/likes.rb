require 'faker'

FactoryBot.define do
  factory :like do

    factory :like_without_user, parent: :like do
      category_id {Category.last.id}
      image_id {Image.last.id}
    end

    factory :like_without_image, parent: :like do
      user_id {User.last.id}
      category_id {Category.last.id}
    end

    factory :like_without_category, parent: :like do
      user_id {User.last.id}
      image_id {Image.last.id}
    end

    factory :like_with_all_associated, parent: :like do
      user_id {User.last.id}
      category_id {Category.last.id}
      image_id {Image.last.id}
    end
  end
end