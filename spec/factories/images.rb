require 'faker'

FactoryBot.define do
  factory :image do

    factory :image_without_category, parent: :image do
      title {Faker::Book.title}
      file_path {Faker::File.file_name}
      user_id {User.last.id}
    end

    factory :image_without_user, parent: :image do
      title {Faker::Book.title}
      file_path {Faker::File.file_name}
      category_id {Category.last.id}
    end

    factory :image_without_title, parent: :image do
      file_path {Faker::File.file_name}
      user_id {User.last.id}
      category_id {Category.last.id}
    end

    factory :image_without_file_path, parent: :image do
      title {Faker::Book.title}
      user_id {User.last.id}
      category_id {Category.last.id}
    end

    # TODO MAKE FILE PATH NOT BLANK. I DON'T KNOW WHY IT'S BLANK.
    factory :image_with_all_associations, parent: :image do
      title {Faker::Book.title}
      file_path do
         Rack::Test::UploadedFile.new(Rails.root.join('public', 'apple-touch-icon.png'), 'image/png')

      end
      user_id {User.last.id}
      category_id {Category.last.id}
    end
  end
end