require 'faker'

FactoryBot.define do
  factory :comment do

    factory :comment_without_user, parent: :comment do
      category_id {Category.last.id}
      image_id {Image.last.id}
      body {Faker::Book.title}
    end

    factory :comment_without_image, parent: :comment do
      user_id {User.last.id}
      category_id {Category.last.id}
      body {Faker::Book.title}
    end

    factory :comment_without_category, parent: :comment do
      user_id {User.last.id}
      image_id {Image.last.id}
      body {Faker::Book.title}
    end

    factory :comment_without_body, parent: :comment do
      user_id {User.last.id}
      category_id {Category.last.id}
      image_id {Image.last.id}
    end

    factory :comment_with_all_important, parent: :comment do
      user_id {User.last.id}
      category_id {Category.last.id}
      image_id {Image.last.id}
      body {Faker::Book.title}
    end
  end
end