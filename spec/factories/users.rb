require 'faker'

FactoryBot.define do
  factory :user do

    factory :user_without_email, parent: :user do
      password {Faker::Internet.password}
      username {Faker::Internet.unique.user_name}
    end

    factory :user_without_password, parent: :user do
      email {Faker::Internet.unique.email}
      username {Faker::Internet.unique.user_name}
    end

    factory :user_without_username, parent: :user do
      email {Faker::Internet.unique.email}
      password {Faker::Internet.password}
    end

    factory :user_with_all_important, parent: :user do
      email {Faker::Internet.unique.email}
      password {Faker::Internet.password}
      username {Faker::Internet.unique.user_name}
    end
  end
end