require 'faker'

FactoryBot.define do

  factory :category do

    factory :category_without_title, parent: :category do
      user_id {build(:user_with_all_important).id}
    end

    factory :category_without_user_id, parent: :category do
      title {Faker::Book.title}
    end

    factory :category_with_all_associations, parent: :category do
      title {Faker::Book.title}
      user_id {User.last.id}
    end
  end
end