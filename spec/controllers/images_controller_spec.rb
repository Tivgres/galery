require 'rails_helper'

RSpec.describe ImagesController, type: :controller do

  before do
    user = build(:user_with_all_important)
    user.save
    allow(controller).to receive(:authenticate_user!).and_return(true)
    allow(controller).to receive(:current_user).and_return(user)
    allow(controller).to receive(:signed_in?).and_return(user)
    build(:user_with_all_important).save
    build(:category_with_all_associations).save
  end

  describe 'GET #index' do
    it 'returns a success response' do
      get :index, params: {}
      expect(response.status).to eq(200)
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      build(:image_with_all_associations).save
      get :show, params: { slug: Image.last.slug }
      expect(response.status).to eq(200)
    end
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get :new, params: {}
      expect(response.status).to eq(200)
    end
  end


  # FOT FIXES HERE
  describe 'POST #create' do
    context 'with valid params' do

      it 'creates a new Image' do
        uploaded_file = Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, '/public/apple-touch-icon.png')))
        expect {
          post :create, params: {image: {title: 'title', category_id: Category.last.id, file_path: uploaded_file}}
        }.to change(Image, :count).by(1)
        expect(response.status).to eq(302)
      end

      it 'creates a new Image WITH ERROR' do
        expect {
          post :create, params: {image: {title: 'title', category_id: Category.last.id}}
        }.to change(Image, :count).by(0)
        expect(response).to redirect_to(new_image_url)
        expect(response.status).to eq(302)
      end
    end

    context 'with invalid params' do
      it 'returns a success response (i.e. to display the new template)' do
        post :create, params: {image: {title: 'title', category_id: Category.last.id}}
        expect(response).to redirect_to(new_image_url)
      end
    end
  end

end
