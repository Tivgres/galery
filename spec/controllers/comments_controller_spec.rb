require 'rails_helper'

RSpec.describe CommentsController, type: :controller do

  before do
    user = build(:user_with_all_important)
    user.save
    allow(controller).to receive(:authenticate_user!).and_return(true)
    allow(controller).to receive(:current_user).and_return(user)
    allow(controller).to receive(:signed_in?).and_return(user)
    build(:user_with_all_important).save
    build(:category_with_all_associations).save
    build(:image_with_all_associations).save
    build(:comment_with_all_important).save
  end

  describe 'GET #index' do
    it 'returns a success response' do
      get :index, params: {}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to eq('text/html; charset=utf-8')
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Comment' do
        @request.env['HTTP_ACCEPT'] = 'text/javascript; charset=utf-8'
        expect {
          post :create, params: { comment: { body: 'qweqweqwe', category_id: Category.last.id, image_id: Image.last.id } }
        }.to change(Comment, :count).by(1)
        expect(response.status).to eq(201)
        expect(response.headers['Content-Type']).to eq('text/javascript; charset=utf-8')
      end
    end

    context 'with invalid params' do
      it 'returns a success response (i.e. to display the new template)' do
        @request.env['HTTP_ACCEPT'] = 'text/javascript; charset=utf-8'
        post :create, params: {comment: { image_id: Image.last.id, user_id: 1111 } }
        expect(response.status).to eq(400)
        expect(response.headers['Content-Type']).to eq('text/javascript; charset=utf-8')
      end
    end
  end

end
