require 'rails_helper'

RSpec.describe CategoriesController, type: :controller do

  before do
    user = build(:user_with_all_important)
    user.save
    build(:category_with_all_associations).save
    allow(controller).to receive(:authenticate_user!).and_return(true)
    allow(controller).to receive(:current_user).and_return(user)
    allow(controller).to receive(:signed_in?).and_return(user)
  end

  let(:category) {
    Category.last
  }


  describe 'GET #index' do
    it 'returns a success response' do
      get :index, params: {}
      expect(response.status).to eq(200)
      expect(assigns(:categories).size).to be 1
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      get :show, params: {slug: category.slug}
      expect(response.status).to eq(200)
      expect(assigns(:category)).to eq(category)
    end

  end

  describe 'GET #new' do
    it 'returns a success response' do
      get :new, params: {}
      expect(response.status).to eq(200)
      expect(assigns(:category).to_param).to eq(Category.new.to_param)
    end
  end

  describe 'GET #edit' do
    it 'returns a success response' do
      get :edit, params: {slug: category.slug}
      expect(response).to have_http_status(200)
      expect(assigns(:category)).to eq(category)
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      let(:category) { { category: { title: 'qwe-qwe' } } }
      it 'creates a new Category' do
        expect {
          post :create, params: {category: {title: 'qwe-qwe'}}
        }.to change(Category, :count).by(1)
      end

      it 'redirects to the created category' do
        post :create, params:  {category: {title: 'qwe-qwe'} }
        expect(response).to redirect_to(Category.last)
      end
    end

    context 'with invalid params' do
      it 'returns a success response (i.e. to display the new template)' do
        post :create, params: {category: {title: ''}}
        expect(response.status).to eq(200)
        expect(response).to render_template(:new)
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do

      it 'updates the requested category' do
        title = 'qwe-qwe'
        put :update, params: {slug: category.to_param, category: {title: title}}
        expect(response).to redirect_to(category_path(category))
        category.reload
        expect(category.title).to eq(title)
      end
    end

    context 'with invalid params' do
      it 'returns a success response (i.e. to display the edit template)' do
        build(:user_with_all_important).save
        category = build(:category_with_all_associations)
        category.save
        put :update, params: {slug: category.to_param, category: {title: 'qweqwe'}}
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested category' do
      expect {
        delete :destroy, params: {slug: category.to_param}
      }.to change(Category, :count).by(-1)
      expect(response).to redirect_to(categories_path)
    end

    it 'redirects to the categories list' do
      delete :destroy, params: {slug: category.to_param}
      expect(response).to redirect_to(categories_url)
    end

    it 'test with wrong user' do
      build(:user_with_all_important).save
      category = build(:category_with_all_associations)
      category.save
      delete :destroy, params: {slug: category.to_param}
      expect(response.status).to eq(403)
    end
  end

end
