require 'rails_helper'

RSpec.describe WelcomeController, type: :controller do

  describe 'GET #index' do
    it 'returns http success BEFORE SIGN IN' do
      get :index
      expect(response.status).to eq(200)
    end

    it 'returns http success AFTER SIGN IN' do
      user = build(:user_with_all_important).save
      allow(controller).to receive(:authenticate_user!).and_return(true)
      allow(controller).to receive(:current_user).and_return(user)
      allow(controller).to receive(:signed_in?).and_return(user)
      get :index
      expect(response.status).to eq(302)
    end
  end

end
