require 'rails_helper'

RSpec.describe LikesController, type: :controller do

  before do
    user = build(:user_with_all_important)
    user.save
    allow(controller).to receive(:authenticate_user!).and_return(true)
    allow(controller).to receive(:current_user).and_return(user)
    allow(controller).to receive(:signed_in?).and_return(user)
    build(:category_with_all_associations).save
    build(:image_with_all_associations).save
    @request.env['HTTP_ACCEPT'] = 'text/javascript; charset=utf-8'
  end

  describe 'POST #create' do
    context 'with valid params' do

      it 'creates a new Like' do
        post :create, params: {like: {category_id: Category.last.id, image_id: Image.last.id}}
        expect(response.headers['Content-Type']).to eq('text/javascript; charset=utf-8')
        expect(response.status).to eq(201)
      end

      it 'creates a new Like TWICE' do
        expect {
          post :create, params: {like: {category_id: Category.last.id, image_id: Image.last.id}}
        }.to change(Like, :count).by(1)
        post :create, params: {like: {category_id: Category.last.id, image_id: Image.last.id}}
        expect(response.headers['Content-Type']).to eq('text/javascript; charset=utf-8')
        expect(response.status).to eq(302)
      end

      it 'creates a new Like WITH FAILS' do
        expect {
          post :create, params: {like: {image_id: Image.last.id}}
        }.to change(Like, :count).by(0)
        expect(response.headers['Content-Type']).to eq('text/javascript; charset=utf-8')
        expect(response.status).to eq(400)
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested like' do
      build(:like_with_all_associated).save
      delete :destroy, params: {id: Like.last.id}
      expect(response.headers['Content-Type']).to eq('text/javascript; charset=utf-8')
      expect(response.status).to eq(202)
    end

    it 'destroys the requested like WITH FAIL' do
      build(:user_with_all_important).save
      build(:like_with_all_associated).save
      delete :destroy, params: {id: Like.last.id}
      expect(response.headers['Content-Type']).to eq('text/javascript; charset=utf-8')
      expect(response.status).to eq(403)
    end
  end

end
