require 'rails_helper'

RSpec.describe "comments/index", type: :view do
  before(:each) do
    build(:user_with_all_important).save
    build(:category_with_all_associations).save
    build(:image_with_all_associations).save
    build(:comment_with_all_important).save
    @comments = Comment.page(params[:page]).per(20)
  end

  it "renders a list of comments" do
    render
  end
end
