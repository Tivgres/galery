require 'rails_helper'

RSpec.describe "images/new", type: :view do
  before(:each) do
    build(:user_with_all_important).save
    build(:category_with_all_associations).save
    @categories = Category.all
    @image = Image.new
    allow(controller).to receive(:authenticate_user!).and_return(true)
    user = User.last
    allow(controller).to receive(:current_user).and_return(user)
    allow(controller).to receive(:signed_in?).and_return(user)
  end

  it "renders new image form" do
    render

    assert_select "form[action=?][method=?]", images_path, "post" do
    end
  end
end
