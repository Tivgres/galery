require 'rails_helper'

RSpec.describe "images/show", type: :view do
  before(:each) do
    allow(controller).to receive(:authenticate_user!).and_return(true)
    build(:user_with_all_important).save
    build(:category_with_all_associations).save
    build(:image_with_all_associations).save
    @image = Image.last
    @comments = @image.comments.order('created_at DESC').page(params[:page]).per(25)
    user = User.last
    allow(controller).to receive(:current_user).and_return(user)
    allow(controller).to receive(:signed_in?).and_return(user)
  end

  it "renders attributes in <p>" do
    render
  end
end
