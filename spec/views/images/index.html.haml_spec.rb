require 'rails_helper'

RSpec.describe "images/index", type: :view do
  before(:each) do
    build(:user_with_all_important).save
    build(:category_with_all_associations).save
    build(:image_with_all_associations).save
    @images = Image.page(params[:page]).per(20)
  end

  it "renders a list of images" do
    render
  end
end
