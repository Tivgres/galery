require 'rails_helper'

RSpec.describe "categories/show", type: :view do
  before(:each) do
    build(:user_with_all_important).save
    allow(controller).to receive(:authenticate_user!).and_return(true)
    build(:category_with_all_associations).save
    user = User.last
    allow(controller).to receive(:current_user).and_return(user)
    @category = Category.last
    @images = @category.images.page(params[:page]).per(6)
  end

  it "renders attributes in <p>" do
    render
  end
end
