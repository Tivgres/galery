require 'rails_helper'

RSpec.describe "categories/edit", type: :view do
  before(:each) do
    build(:user_with_all_important).save
    build(:category_with_all_associations).save
    @category = assign(:category, Category.last)
  end

  it "renders the edit category form" do
    render

    assert_select "form[action=?][method=?]", category_path(@category), "post" do
    end
  end
end
