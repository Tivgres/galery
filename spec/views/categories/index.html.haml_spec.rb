require 'rails_helper'

RSpec.describe "categories/index", type: :view do
  before(:each) do
    build(:user_with_all_important).save
    allow(controller).to receive(:authenticate_user!).and_return(true)
    user = User.last
    @categories = Category.page(params[:page]).per(20)
    allow(controller).to receive(:current_user).and_return(user)
    allow(controller).to receive(:signed_in?).and_return(user)
  end

  it "renders a list of categories" do
    render
  end
end
