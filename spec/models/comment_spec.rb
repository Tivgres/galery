require 'rails_helper'

RSpec.describe Comment, type: :model do

  before do
    build(:user_with_all_important).save
    build(:category_with_all_associations).save
    build(:image_with_all_associations).save
  end

  let(:comment) { build(:comment_with_all_important) }

  describe 'relationship test' do
    it 'belongs_to category' do
      expect(comment).to respond_to :category
    end
    it 'belongs_to user' do
      expect(comment).to respond_to :user
    end
    it 'belongs_to image' do
      expect(comment).to respond_to :image
    end
  end

  describe 'validation test' do
    it 'ensures category presence' do
      expect(build(:comment_without_user).save).to be_falsey
    end
    it 'ensures category presence' do
      expect(build(:comment_without_image).save).to be_falsey
    end
    it 'ensures category presence' do
      expect(build(:comment_without_category).save).to be_falsey
    end
    it 'ensures body presence' do
      expect(build(:comment_without_body).save).to be_falsey
    end
    # it 'successfully saving' do
    #   expect(build(:comment_with_all_important).save).to be_truthy
    # end
  end

end
