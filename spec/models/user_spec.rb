require 'rails_helper'

RSpec.describe User, type: :model do

  let(:user) { build(:user_with_all_important) }

  github_oauth = %{#<OmniAuth::AuthHash credentials=#<OmniAuth::AuthHash expires=false token="76ce76d849874d3c76d5417abf"> extra=#<OmniAuth::AuthHash all_emails=#<Hashie::Array []> raw_info=#<OmniAuth::AuthHash avatar_url="https://avatars3.githubusercontent.com/u/24366744?v=4" bio=nil blog="https://tivgres.me" company=nil created_at="2016-12-04T07:41:58Z" email="mr.serg.vit@gmail.com" events_url="https://api.github.com/users/Tivgres/events{/privacy}" followers=1 followers_url="https://api.github.com/users/Tivgres/followers" following=2 following_url="https://api.github.com/users/Tivgres/following{/other_user}" gists_url="https://api.github.com/users/Tivgres/gists{/gist_id}" gravatar_id="" hireable=nil html_url="https://github.com/Tivgres" id=24366744 location="Ukraine" login="Tivgres" name="name" organizations_url="https://api.github.com/users/Tivgres/orgs" public_gists=0 public_repos=7 received_events_url="https://api.github.com/users/Tivgres/received_events" repos_url="https://api.github.com/users/Tis/repos" site_admin=false starred_url="https://api.github.com/users/es/starred{/owner}{/repo}" subscriptions_url="https://api.github.com/users/Tivgres/subscriptions" type="User" updated_at="2018-05-22T09:18:41Z" url="https://api.github.com/users/Tivgres">> info=#<OmniAuth::AuthHash::InfoHash email="mr.serg.vit@gmail.com" image="https://avatars3.githubusercontent.com/u/24366744?v=4" name="Sergey" nickname="Tes" urls=#<OmniAuth::AuthHash Blog="https://tivgres.me" GitHub="https://github.com/ttt">> provider="github" uid="243744">}
  twitter_oauth = %{#<OmniAuth::AuthHash credentials=#<OmniAuth::AuthHash secret="4wQgg6xtpHW6v2S7JhwUO5ZoaqKtm3QAJCh6LkZAeNOnW" token="994929188384-qL1jV4NckxhaOkCemMwHi0cLkU"> extra=#<OmniAuth::AuthHash access_token=#<OAuth::AccessToken:0x00000005202628 @token="994927753484-qL1jV4NciH7aOkCemMwHi0cLkU", @secret="4wQgg6xtpHW6v2S7JhwUO5ZoaqKtm3QAJCh6LkZAeNOnW", @consumer=#<OAuth::Consumer:0x000000053adbd0 @key="6HUfQaXcxak5gEX98xBNI4g5y", @secret="e4ZlsAwykB1joYYu01yOmgSad9fH4kWzMzofTlXO9Wjnaj1Puh", @options={:signature_method=>"HMAC-SHA1", :request_token_path=>"/oauth/request_token", :authorize_path=>"/oauth/authenticate", :access_token_path=>"/oauth/access_token", :proxy=>nil, :scheme=>:header, :http_method=>:post, :debug_output=>nil, :oauth_version=>"1.0", :site=>"https://api.twitter.com"}, @debug_output=nil, @http=#<Net::HTTP api.twitter.com:443 open=false>, @http_method=:post, @uri=#<URI::HTTPS https://api.twitter.com>>, @params={:oauth_token=>"994929107753488384-qL1jV4NckxhXziH7aOkCemMwHi0cLkU", "oauth_token"=>"994929107753488384-qLziH7aOHi0cLkU", :oauth_token_secret=>"4wQgg6xtJhwUO5ZoaqKtm3QAJCh6LkZAeNOnW", "oauth_token_secret"=>"4wQgg6xtpHWoaqKtm3QAJCh6LkZAeNOnW", :user_id=>"999107753488384", "user_id"=>"994929107753488384", :screen_name=>"tivgres1", "screen_name"=>"tivgres1"}, @response=#<Net::HTTPOK 200 OK readbody=true>> raw_info=#<OmniAuth::AuthHash contributors_enabled=false created_at="Fri May 11 13:15:59 +0000 2018" default_profile=true default_profile_image=true description="" entities=#<OmniAuth::AuthHash description=#<OmniAuth::AuthHash urls=#<Hashie::Array []>>> favourites_count=0 follow_request_sent=false followers_count=0 following=false friends_count=0 geo_enabled=false has_extended_profile=false id=994929107753488384 id_str="994929107753488384" is_translation_enabled=false is_translator=false lang="en" listed_count=0 location="" name="tivgres" notifications=false profile_background_color="F5F8FA" profile_background_image_url=nil profile_background_image_url_https=nil profile_background_tile=false profile_image_url="http://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png" profile_image_url_https="https://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png" profile_link_color="1DA1F2" profile_sidebar_border_color="C0DEED" profile_sidebar_fill_color="DDEEF6" profile_text_color="333333" profile_use_background_image=true protected=false screen_name="tivgres1" statuses_count=0 time_zone=nil translator_type="none" url=nil utc_offset=nil verified=false>> info=#<OmniAuth::AuthHash::InfoHash description="" email=nil image="http://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png" location="" name="tivgres" nickname="tivgr" urls=#<OmniAuth::AuthHash Twitter="https://twitter.com/tivgres1" Website=nil>> provider="twitter" uid="9949253484">}
  google_oauth = %{#<OmniAuth::AuthHash credentials=#<OmniAuth::AuthHash expires=true expires_at=1527091208 token="ya29.GlzEBTi4_WoZXBhXFMJKasmflbmvixuxheu_o0a8gMiunqPfMGcacwrSlSYVbnXii7IGj4zn_eJ_9wlCFM-eEaPwioNoAEbFdNWLl23kxnccIg"> extra=#<OmniAuth::AuthHash id_info=#<OmniAuth::AuthHash at_hash="xzCaP6UXMQEeof9azzksaQ" aud="966875777726-ui6p153o2j33ui0pd5mdlfgcvui0vv3q.apps.googleusercontent.com" azp="966875777726-ui6p153o2j33ui0pd5mdlfgcvui0vv3q.apps.googleusercontent.com" email="mr.serg.d@gmail.com" email_verified=true exp=1527091208 iat=1527087608 iss="accounts.google.com" sub="111649372577954575182"> id_token="eyJhbGciOiJSUzI1NiIsImtpZCI6IjAyOWYyNjlmZXJpZmllZCI6dHJ1ZSwiYXRfaGFzaCI6Inh6Q2FQNlVYTVFFZW9mOWF6emtzYVEiLCJleHAiOjE1MjcwOTEyMDgsImlzcyI6ImFjY291bnRzLmdvb2dsZS5jb20iLCJpYXQiOjE1MjcwODc2MDh9.mBHnTQzlYTiI_8u9La3959By7mb5Z-L1YFRr7m3147aQvLC_SrOEEJxzdTc-2vVpReVLEo0teWSsgIqm4vRWwDAwr2VBmv5pBIsURy40T6KGhG0syxJV8y05UhTOdIyx8EP5DIT_Iv-IMtZRYf1N-loCgKsBfNmtr8rPmrXmibqVYOkaw-MKAqxTLmjPOmLVW9TpWT7-Gd5twFgSMr-rhTyfWHyDbm0rXiQE9yZ5yFqEuqbv3XIS13VDQYbVR3n1khRY7a7Pl7xM6iwBNZAsstmIgMIKAtAD_Y14pS5G8PnAXCg4aDVVtPA_f0heF7Oep1IWF94QMOkck01WPZjEfg" raw_info=#<OmniAuth::AuthHash email="mr.serg.d@gmail.com" email_verified="true" family_name="" given_name="Sergey" kind="plus#personOpenIdConnect" locale="en" name="Sergey " picture="https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50" sub="111649372577954575182">> info=#<OmniAuth::AuthHash::InfoHash email="mr.serg.d@gmail.com" first_name="Sergey" image="https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg" last_name="" name="Sergey Dedik"> provider="google_oauth2" uid="1116493725775182">}

  describe 'relationship test' do
    it 'has_many categories' do
      expect(user).to respond_to :categories
      # This is not works :(
      # expect(user.categories).to be_an_instance_of(Category::ActiveRecord_Associations_CollectionProxy)
      # expect(user.categories.first).to be_an_instance_of(Category)
    end
    it 'has_many subscribes' do
      expect(user).to respond_to :subscribes
      # expect(user.subscribes.first).to be_an_instance_of(Subscribe)
    end
    it 'has_many images' do
      expect(user).to respond_to :images
      # expect(user.images.first).to be_an_instance_of(Image)
    end
    it 'has_many comments' do
      expect(user).to respond_to :comments
      # expect(user.comments.first).to be_an_instance_of(Comment)
    end
    it 'has_many likes' do
      expect(user).to respond_to :likes
      # expect(user.likes.first).to be_an_instance_of(Like)
    end
  end

  describe 'validation test' do
    it 'ensures email presence' do
      expect(build(:user_without_password).save).to be_falsey
    end
    it 'ensures password presence' do
      expect(build(:user_without_email).save).to be_falsey
    end
    it 'ensures username presence' do
      expect(build(:user_without_username).save).to be_falsey
    end
    it 'successfully saving' do
      expect(build(:user_with_all_important).save).to be_truthy
    end
  end

  describe 'oauth test' do
    it 'test github oauth without exist user' do
      expect { User.from_omniauth OmniAuth.config.mock_auth[:github]
      }.to change(User, :count).by(1) && change(Account, :count).by(1)
    end

    it 'test github oauth with exist user' do
      User.create(email: 'mr.serg.vit@gmail.com', password: 'secret')
      expect { User.from_omniauth OmniAuth.config.mock_auth[:github]
      }.to change(User, :count).by(0) && change(Account, :count).by(1)
    end

    it 'test twitter oauth without exist user' do
      expect { User.from_omniauth OmniAuth.config.mock_auth[:twitter]
      }.to change(User, :count).by(1) && change(Account, :count).by(1)
    end

    it 'test twitter oauth with exist user' do
      User.create(email: 'tivgr@twitter.com', password: 'secret')
      expect { User.from_omniauth OmniAuth.config.mock_auth[:twitter]
      }.to change(User, :count).by(0) && change(Account, :count).by(1)
    end

    it 'test google oauth without exist user' do
      expect { User.from_omniauth OmniAuth.config.mock_auth[:google]
      }.to change(User, :count).by(1) && change(Account, :count).by(1)
    end

    it 'test google oauth with exist user' do
      User.create(email: 'mr.serg.d@gmail.com', password: 'secret')
      expect { User.from_omniauth OmniAuth.config.mock_auth[:google]
      }.to change(User, :count).by(0) && change(Account, :count).by(1)
    end

  end

end
