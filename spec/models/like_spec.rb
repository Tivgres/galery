require 'rails_helper'

RSpec.describe Like, type: :model do

  before do
    build(:user_with_all_important).save
    build(:category_with_all_associations).save
    build(:image_with_all_associations).save
  end

  let(:like) { build(:like_with_all_associated) }

  describe 'relationship test' do
    it 'belongs_to category' do
      expect(like).to respond_to :category
    end
    it 'belongs_to user' do
      expect(like).to respond_to :user
    end
    it 'belongs_to image' do
      expect(like).to respond_to :image
    end
  end

  describe 'validation test' do
    it 'ensures category presence' do
      expect(build(:like_without_user).save).to be_falsey
    end
    it 'ensures category presence' do
      expect(build(:like_without_image).save).to be_falsey
    end
    it 'ensures category presence' do
      expect(build(:like_without_category).save).to be_falsey
    end
    it 'successfully saving' do
      expect(like.save).to be_truthy
    end
  end
end
