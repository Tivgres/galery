require 'rails_helper'

RSpec.describe Category, type: :model do

  before do
    build(:user_with_all_important).save
  end

  let(:category) { build(:category_with_all_associations) }

  describe 'relationship test' do
    it 'has_many images' do
      expect(category).to respond_to :images
    end
    it 'has_many likes' do
      expect(category).to respond_to :likes
    end
    it 'has_many comments' do
      expect(category).to respond_to :comments
    end
    it 'has_many subscribes' do
      expect(category).to respond_to :subscribes
    end
    it 'belongs_to user' do
      expect(category).to respond_to :user
    end
  end

  describe 'validation test' do
    it 'ensures title presence' do
      expect(build(:category_without_title).save).to be_falsey
    end
    it 'ensures user_id presence' do
      expect(build(:category_without_user_id).save).to be_falsey
    end
    it 'successfully saving' do
      build(:user_with_all_important).save
      expect(category.save).to be_truthy
    end
  end

end
