require 'rails_helper'

RSpec.describe Image, type: :model do

  before do
    build(:user_with_all_important).save
    build(:category_with_all_associations).save
  end

  let(:image) { build(:image_with_all_associations) }

  describe 'relationship test' do
    it 'belongs_to category' do
      expect(image).to respond_to :category
    end
    it 'belongs_to user' do
      expect(image).to respond_to :user
    end
    it 'has_many comments' do
      expect(image).to respond_to :comments
    end
    it 'has_many likes' do
      expect(image).to respond_to :likes
    end
  end

  describe 'validation test' do
    it 'ensures category presence' do
      expect(build(:image_without_category).save).to be_falsey
    end
    it 'ensures title presence' do
      expect(build(:image_without_title).save).to be_falsey
    end
    it 'ensures file_path presence' do
      expect(build(:image_without_file_path).save).to be_falsey
    end
    it 'ensures user presence' do
      expect(build(:image_without_user).save).to be_falsey
    end
    it 'successfully saving' do
      expect(image.save!).to be_truthy
    end
  end
end
